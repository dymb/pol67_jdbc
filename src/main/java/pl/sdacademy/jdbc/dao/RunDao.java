package pl.sdacademy.jdbc.dao;

import pl.sdacademy.jdbc.entity.Run;

import java.sql.SQLException;
import java.util.List;

public interface RunDao {
    // C
    void save(Run run) throws SQLException;

    // R
    List<Run> findAll() throws SQLException;
    Run findById(int id) throws SQLException;

    // U
    void update(Run run) throws SQLException;

    // D
    void delete(int id) throws SQLException;
}
