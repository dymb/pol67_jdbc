package pl.sdacademy.jdbc.daoimpl;

import pl.sdacademy.jdbc.dao.RunDao;
import pl.sdacademy.jdbc.entity.Run;
import pl.sdacademy.jdbc.utils.JdbcUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class RunDaoImpl implements RunDao {
    public void save(Run run) throws SQLException {
        Connection connection = JdbcUtils
                .getInstance()
                .getConnection();
        PreparedStatement statement = connection
                .prepareStatement("INSERT INTO RUNS (id, name, members_limit) VALUES (?, ?, ?)");

        statement.setInt(1, run.getId());
        statement.setString(2, run.getName());
        statement.setInt(3, run.getMembersLimit());

        statement.executeUpdate();

        statement.close();
    }

    public Run findById(int id) throws SQLException {
        Connection connection = JdbcUtils
                .getInstance()
                .getConnection();

        PreparedStatement statement = connection
                .prepareStatement("SELECT * FROM RUNS WHERE id=?");
        statement.setInt(1, id);

        ResultSet result = statement.executeQuery();

        Run run = null;
        if (result.next()) {
            run = new Run();
            run.setId(result.getInt("id"));
            run.setName(result.getString("name"));
            run.setMembersLimit(result.getInt("members_limit"));
        }

        statement.close();

        return run;
    }

    public List<Run> findAll() throws SQLException {
        return null;
    }

    public void update(Run run) throws SQLException {

    }

    public void delete(int id) throws SQLException {

    }
}
