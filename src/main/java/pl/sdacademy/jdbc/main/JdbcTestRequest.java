package pl.sdacademy.jdbc.main;

import pl.sdacademy.jdbc.utils.JdbcUtils;

import java.sql.*;

public class JdbcTestRequest {
    public static void main(String[] args) throws SQLException {
        System.out.println(getMaxRunId());

        insertRuns("Bieg numer 1", 100);
        insertRuns("Bieg numer 2", 60);
        deleteById(3);

        updateRun(5, "Bieg Solidatnosci", 200);
       // printAllData();
        findByNameFragment("numer");
    }

    private static Integer getMaxRunId() throws SQLException {
        Connection connection = JdbcUtils
                .getInstance()
                .getConnection();
        Statement statement = connection.createStatement();
        ResultSet result = statement.executeQuery("SELECT MAX(id) as maxId FROM RUNS");

        result.next();
        int maxId = result.getInt("maxId");

        statement.close();

        return maxId;
    }

    private static void insertRuns(String name, int membersLimit) throws SQLException {
        Connection connection = JdbcUtils
                .getInstance()
                .getConnection();
        PreparedStatement statement = connection
                .prepareStatement("INSERT INTO RUNS (id, name, members_limit) VALUES (?, ?, ?)");

        statement.setInt(1, getMaxRunId() + 1);
        statement.setString(2, name);
        statement.setInt(3, membersLimit);

        statement.executeUpdate();

        statement.close();
    }

    private static void deleteById(int runId) throws SQLException {
        Connection connection = JdbcUtils
                .getInstance()
                .getConnection();
        PreparedStatement statement = connection
                .prepareStatement("DELETE FROM RUNS WHERE id=?");

        statement.setInt(1, runId);

        statement.executeUpdate();

        statement.close();
    }

    private static void updateRun(int id, String name, int membersLimit) throws SQLException {
        Connection connection = JdbcUtils
                .getInstance()
                .getConnection();
        PreparedStatement statement = connection
                .prepareStatement("UPDATE RUNS SET name=?, members_limit=? WHERE id=?");
        statement.setString(1, name);
        statement.setInt(2, membersLimit);
        statement.setInt(3, id);
        statement.executeUpdate();
        statement.close();
    }

    private static void printAllData() throws SQLException {
        Connection connection = JdbcUtils
                .getInstance()
                .getConnection();
        Statement statement = connection.createStatement();
        ResultSet result = statement.executeQuery("SELECT * FROM RUNS");
        while (result.next()) {
            System.out.println(
                    "Id:" + result.getInt("id") +
                    " Name: " + result.getString("name") +
                    " Members limit: " + result.getInt("members_limit"));
        }
        statement.close();
    }

    private static void findByNameFragment(String fragment) throws SQLException {
        Connection connection = JdbcUtils
                .getInstance()
                .getConnection();

        PreparedStatement statement = connection
                .prepareStatement("SELECT * FROM RUNS WHERE name LIKE ?");
        statement.setString(1, "%" + fragment + "%");

        ResultSet result = statement.executeQuery();
        while (result.next()) {
            System.out.println(
                    "Id:" + result.getInt("id") +
                            " Name: " + result.getString("name") +
                            " Members limit: " + result.getInt("members_limit"));
        }

        statement.close();
    }

}
