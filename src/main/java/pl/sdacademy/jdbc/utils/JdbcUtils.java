package pl.sdacademy.jdbc.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class JdbcUtils {
    private Connection connection;

    private static JdbcUtils instance;

    public static JdbcUtils getInstance() {
        if(instance == null) {
            instance = new JdbcUtils();
        }
        return instance;
    }

    private JdbcUtils() {
        String connectionString = "jdbc:mysql://localhost:3306/runcenter";
        Properties prop = new Properties();
        prop.put("password", "1haslo1");
        prop.put("user", "root");
        prop.put("serverTimezone", "Europe/Warsaw");
        try {
            connection = DriverManager.getConnection(connectionString, prop);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public Connection getConnection() {
        return connection;
    }
}
