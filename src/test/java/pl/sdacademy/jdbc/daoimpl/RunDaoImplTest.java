package pl.sdacademy.jdbc.daoimpl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.sdacademy.jdbc.dao.RunDao;
import pl.sdacademy.jdbc.entity.Run;
import pl.sdacademy.jdbc.utils.JdbcUtils;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class RunDaoImplTest {

    private RunDao runDao = new RunDaoImpl();

    @BeforeEach
    void cleanTable() throws SQLException {
        Connection connection = JdbcUtils
                .getInstance()
                .getConnection();
        Statement statement = connection.createStatement();
        statement.executeUpdate("DELETE FROM RUNS");
        statement.close();
    }

    @Test
    void save() throws SQLException {
        //Given
        Run run = new Run();
        run.setId(1);
        run.setName("Testowa nazwa 1");
        run.setMembersLimit(10);

        //WHEN
        runDao.save(run);
        Run saved = runDao.findById(run.getId());

        //THEN
        assertNotNull(saved);
        assertEquals(run.getId(), saved.getId());
        assertEquals(run.getName(), saved.getName());
        assertEquals(run.getMembersLimit(), saved.getMembersLimit());
    }

    @Test
    void update() throws SQLException {
        //Given
        Run run = new Run();
        run.setId(10);
        run.setName("test przed update");
        run.setMembersLimit(1000);

        //When
        runDao.save(run);
        run.setMembersLimit(99);
        run.setName("Nazwa inna");

        runDao.update(run);

        Run updated = runDao.findById(run.getId());

        assertNotNull(updated);
        assertEquals(run.getName(), updated.getName());
        assertEquals(run.getMembersLimit(), updated.getMembersLimit());
    }

    @Test
    void delete() throws SQLException {
        //Given
        Run run = new Run();
        run.setId(1);
        run.setName("Testowa nazwa 1");
        run.setMembersLimit(10);

        //WHEN
        runDao.save(run);
        runDao.delete(run.getId());

        Run deleted = runDao.findById(run.getId());

        assertNull(deleted);
    }

    @Test
    void findAll() throws SQLException {
        Run run1 = new Run();
        run1.setId(1);
        run1.setName("Bieg pierwszy");
        run1.setMembersLimit(1032);

        Run run2 = new Run();
        run2.setId(2);
        run2.setName("Inny bieg testowy");
        run2.setMembersLimit(100);

        runDao.save(run1);
        runDao.save(run2);

        List<Run> list = runDao.findAll();
        assertNotNull(list);
        assertEquals(2, list.size());
    }
}